# Terraform Up And Running, (2nd Edition) Learning - chapter 4

Chapter 4 introduces modules, making the code reusable across environments.

1. We add a production environment.
2. The staging environment uses fewer / smaller resources by passing configuration into the module(s).
3. Separate 3 buckets (backends) for the state in 2 environments, `staging` and `production`.  
The quote below is from a [blog post](https://blog.gruntwork.io/how-to-manage-terraform-state-28f5697e68fa) by the books authors.

> Therefore, I recommend using separate Terraform folders (and therefore separate state files) for each environment (staging, production, etc.) and for each component (vpc, services, databases).

## Instructions

1. Provision remote state in global/s3
2. Terraform mysql in staging/data-stores/mysql
3. Terraform webserver in services/webserver-cluster

### Steps

1. Log in to the AWS console
1. Create a default VPC, if one doesn't exist
1. Export aws access key vars
    1. `export AWS_ACCESS_KEY_ID=(your access key id)`
    1. `export AWS_SECRET_ACCESS_KEY=(your secret access key)`
1. Export `TF_VARS_*` vars.  
__WARNING:__ These are for *ease of use* and the DB password should __NOT__ be here.  
Use a secret store instead!
    1. `export TF_VAR_bucket_name=staging-s3-terraform-state`
    1. `export TF_VAR_table_name=staging-ddb-terraform-locks`
    1. `export TF_VAR_db_password=Jt5ZJVc5aNEwCz`
1. `cd global/s3`
    1. `terraform init`
    1. `terraform plan`
    1. `terraform apply -auto-approve`
1. `cd ../../staging/data-stores/mysql`
    1. `terraform init -backend-config=../../backend.hcl`
    1. `terraform apply -auto-approve`
1. `cd ../../services/webserver-cluster`
    1. `terraform init -backend-config=../../backend.hcl`
    1. `terraform apply -auto-approve`
1. `cd ../../../production/data-stores/mysql/`
    1. `terraform init -backend-config=../../backend.hcl`
    1. `terraform apply -auto-approve`
1. `cd ../../services/webserver-cluster`
    1. `terraform init -backend-config=../../backend.hcl`
    1. `terraform apply -auto-approve`

## TODO

1. Use separate VPCS for production and staging
1. Use separate backend states (buckets, DDB tables) for production and staging
1. Review <https://terragrunt.gruntwork.io/> - gets rid of duplication
